# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

ARG ALPINE_VERSION="latest"
ARG TARGET_ARCH="library"

FROM index.docker.io/${TARGET_ARCH}/alpine:${ALPINE_VERSION}

LABEL maintainer="Olliver Schinagl <oliver@schinagl.nl>"

RUN \
    apk add --no-cache \
        alpine-sdk \
    && \
    adduser root abuild && \
    adduser -D -G abuild buildbot && \
    chgrp abuild "/etc/apk/keys/" && \
    chmod g+w "/etc/apk/keys"

# TODO #3 Remove this once 'large-dependency-buffer' apk version is released to latest
# As apk-tools is already installed, we need to go through an upgrade
# hadolint ignore=DL3017
RUN \
    apk upgrade --no-cache \
        apk-tools \
        --repository="$(sed '1 s|^\(.*alpine/\)\(.*\)$|\1edge/main|;2,$d' "/etc/apk/repositories")"

COPY "./bin/package_builder-alpine.sh" "/usr/local/bin/package_builder-alpine.sh"
COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
COPY "./dockerfiles/docker-entrypoint.sh" "/init"

ENTRYPOINT [ "/init" ]
